#pragma once

template<class T>
class BufferQueue {
 public:
  const int max_size;

 private:
  T* buffer = nullptr;

  int first = 0;
  int last = 0;

 public:
  explicit BufferQueue(int max_size);
  ~BufferQueue();

  void push_back(T);
  void pop_front();
  inline constexpr T get() const;
  inline constexpr int size() const;
  inline constexpr void clear();
  inline constexpr bool is_empty() const;
};

#include "BufferQueue.inl"
