#pragma once

#include "BufferQueue.h"

template<class T>
class Iterator {
 private:
  BufferQueue<T>* buffer = nullptr;
  int curr = 0;

 public:
  explicit Iterator(BufferQueue<T>&);

  inline constexpr void start();
  inline constexpr void next();
  inline constexpr bool finish() const;
  inline constexpr int get_value() const;
};

#include "Iterator.inl"
