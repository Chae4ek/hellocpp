#pragma once

#include <iostream>

#include "BufferQueue.h"

template<class T>
BufferQueue<T>::BufferQueue(int max_size) : max_size(max_size) {
  buffer = new int[++max_size];
}

template<class T>
BufferQueue<T>::~BufferQueue() {
  delete[] buffer;
  buffer = nullptr;
}

template<class T>
void BufferQueue<T>::push_back(T element) {
  if (last - first == max_size || last == first - 1) throw std::overflow_error("error: buffer is full");
  buffer[last] = element;
  if (++last == max_size + 1) last = 0;
}

template<class T>
void BufferQueue<T>::pop_front() {
  if (first == last) throw std::underflow_error("error: buffer is empty");
  if (++first == max_size + 1) first = 0;
}

template<class T>
inline constexpr T BufferQueue<T>::get() const {
  return buffer[first];
}

template<class T>
inline constexpr int BufferQueue<T>::size() const {
  return last >= first ? last - first : max_size - first + 1 + last;
}

template<class T>
inline constexpr void BufferQueue<T>::clear() {
  first = last = 0;
}

template<class T>
inline constexpr bool BufferQueue<T>::is_empty() const {
  return first == last;
}
