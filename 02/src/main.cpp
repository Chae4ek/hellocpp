#include <iostream>

#include "BufferQueue.h"
#include "Iterator.h"

void demo() {
  BufferQueue<int> buffer(3);
  buffer.push_back(1);
  buffer.push_back(2);
  buffer.push_back(3);
  std::cout << buffer.max_size << std::endl;

  Iterator<int> it(buffer);

  it.start();
  while (!it.finish()) {
    int t = it.get_value();
    std::cout << t << std::endl;
    it.next();
  }

  std::cout << "  " << buffer.size() << std::endl;

  it.start();
  while (!it.finish()) {
    int t = it.get_value();
    std::cout << t << std::endl;
    it.next();
  }

  std::cout << "  " << buffer.is_empty() << std::endl;
  buffer.clear();
  std::cout << "  " << buffer.is_empty() << std::endl;
}

int main() {
  demo();
  return 0;
}
