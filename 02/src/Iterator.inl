#pragma once

#include "Iterator.h"

template<class T>
Iterator<T>::Iterator(BufferQueue<T>& buffer) : buffer(&buffer) {}

template<class T>
inline constexpr void Iterator<T>::start() {
  curr = buffer->size();
}

template<class T>
inline constexpr void Iterator<T>::next() {
  T temp = buffer->get();
  buffer->pop_front();
  buffer->push_back(temp);
  --curr;
}

template<class T>
inline constexpr bool Iterator<T>::finish() const {
  return !curr;
}

template<class T>
inline constexpr int Iterator<T>::get_value() const {
  return buffer->get();
}
