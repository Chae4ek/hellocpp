#include <iostream>

#include "Dictionary.h"

void demo() {
  Dictionary d;

  d.put("3");
  d.put("1");
  d.put("2");
  d.put("4");  //
  d.put("9");
  d.put("8");
  d.put("7");
  d.put("4");  //

  std::cout << d << std::endl;

  std::cout << "'4' count is: " << d.count_of("4") << ", all words count: " << d.get_words_count()
            << std::endl;
  d.pop("4");
  std::cout << "'4' count is: " << d.count_of("4") << ", all words count: " << d.get_words_count()
            << std::endl;
  d.pop("4");
  std::cout << "'4' count is: " << d.count_of("4") << ", all words count: " << d.get_words_count()
            << std::endl;

  std::cout << "dictionary = " << d << std::endl;

  d.pop("9");
  std::cout << "dictionary = " << d << std::endl;
  d.pop("3");
  std::cout << "dictionary = " << d << std::endl;
  d.pop("8");
  std::cout << "dictionary = " << d << std::endl;
  d.pop("1");
  std::cout << "dictionary = " << d << std::endl;
  d.pop("7");
  std::cout << "dictionary = " << d << std::endl;
  d.pop("2");
  std::cout << "dictionary = " << d << std::endl;
}

int main() {
  demo();
  return 0;
}
