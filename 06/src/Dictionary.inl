#pragma once

#include <iostream>

#include "Dictionary.h"

Dictionary::~Dictionary() {
  if (root) delete root;  // Node destructor is invoked
}

Dictionary::Dictionary(const Dictionary& d) {
  _fast_copy_tree(d.root, root);
  words_count = d.words_count;
}

Dictionary::Dictionary(Dictionary&& d) {
  root = std::move(d.root);
  d.root = nullptr;
  words_count = std::move(d.words_count);
}

Dictionary& Dictionary::operator=(const Dictionary& d) {
  if (this == &d) return *this;
  Dictionary temp(d);
  std::swap(root, temp.root);
  std::swap(words_count, temp.words_count);
  return *this;
}

Dictionary& Dictionary::operator=(Dictionary&& d) {
  if (this == &d) return *this;
  std::swap(root, d.root);
  std::swap(words_count, d.words_count);
  return *this;
}

void Dictionary::put(const std::string& word) {
  Node* curr = root;
  Node* prev = nullptr;
  while (curr && curr->word != word) {
    prev = curr;

    if (word < curr->word)
      curr = curr->left;
    else
      curr = curr->right;
  }
  ++words_count;

  if (curr)
    ++curr->count;
  else if (prev) {
    if (word < prev->word)
      prev->left = new Node(word);
    else
      prev->right = new Node(word);
  } else
    root = new Node(word);
}

void Dictionary::pop(const std::string& word) {
  Node* curr = root;
  Node* prev = nullptr;
  while (curr && curr->word != word) {
    prev = curr;

    if (word < curr->word)
      curr = curr->left;
    else
      curr = curr->right;
  }

  if (curr) {
    --words_count;
    if (curr->count > 1)
      --curr->count;
    else {
      Node** branch_to_delete = !prev ? &root : prev->left == curr ? &prev->left : &prev->right;
      Node* l = curr->left;
      Node* r = curr->right;

      curr->left = nullptr;
      curr->right = nullptr;
      delete curr;

      if (!l)
        *branch_to_delete = r;
      else
        *branch_to_delete = l;

      if (l && r) {
        while (true) {
          Node* next_l = l->right;
          Node* next_r = r->left;

          if (!r) break;
          l->right = r;
          if (!next_l) break;
          r->left = next_l;

          l = next_l;
          r = next_r;
        }
      }
    }
  }
}

std::size_t Dictionary::count_of(std::string word) const {
  Node* curr = root;
  while (curr && curr->word != word) {
    if (word < curr->word)
      curr = curr->left;
    else
      curr = curr->right;
  }
  return curr ? curr->count : 0;
}

inline constexpr std::size_t Dictionary::get_words_count() const {
  return words_count;
}

void Dictionary::_fast_copy_tree(const Node* fromRoot, Node*& toRoot) {
  if (!fromRoot) return;
  toRoot = new Node(fromRoot->word);
  toRoot->count = fromRoot->count;
  _fast_copy_tree(fromRoot->left, toRoot->left);
  _fast_copy_tree(fromRoot->right, toRoot->right);
}

template<class Function>
void Dictionary::_fast_for_each(Node* root, Function func) const {
  if (!root) return;
  _fast_for_each(root->left, func);
  func(root);
  _fast_for_each(root->right, func);
}

std::ostream& operator<<(std::ostream& out, const Dictionary& d) {
  d._fast_for_each(d.root, [&](Dictionary::Node* r) { out << r->word << ' '; });
  return out;
}
