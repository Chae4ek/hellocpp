#pragma once

#include <ostream>
#include <string>

class Dictionary {
 private:
  struct Node {
    std::string word;
    int count;
    Node* left = nullptr;
    Node* right = nullptr;
    Node(std::string word, Node* left = nullptr, Node* right = nullptr)
        : word(word), count(1), left(left), right(right) {}
    ~Node() {
      if (left) delete left;
      if (right) delete right;
    }
  };

  Node* root = nullptr;
  std::size_t words_count = 0;

 public:
  Dictionary() = default;
  ~Dictionary();
  Dictionary(const Dictionary&);
  Dictionary(Dictionary&&);

  Dictionary& operator=(const Dictionary&);
  Dictionary& operator=(Dictionary&&);

  void put(const std::string& word);
  void pop(const std::string& word);
  std::size_t count_of(std::string word) const;
  inline constexpr std::size_t get_words_count() const;

  friend std::ostream& operator<<(std::ostream&, const Dictionary&);

 private:
  void _fast_copy_tree(const Node* fromRoot, Node*& toRoot);
  std::size_t _fast_find(Node* root, const std::string& word) const;

  template<class Function>
  void _fast_for_each(Node*, Function) const;
};

#include "Dictionary.inl"
