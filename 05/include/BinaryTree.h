#pragma once

#include <ostream>
#include <vector>

template<class T>
class BinaryTree {
  static_assert(std::is_same_v<T, short> || std::is_same_v<T, int> || std::is_same_v<T, long> ||
                    std::is_same_v<T, long long>,
                "support only short, int, long or long long");

 private:
  struct Node {
    T element;
    Node* left = nullptr;
    Node* right = nullptr;
    Node(T element, Node* left = nullptr, Node* right = nullptr)
        : element(element), left(left), right(right) {}
    ~Node() {
      if (left) delete left;
      if (right) delete right;
    }
  };

  Node* root = nullptr;
  std::size_t tree_size = 0;

 public:
  BinaryTree();
  ~BinaryTree();
  BinaryTree(const BinaryTree&);
  BinaryTree(BinaryTree&&);

  BinaryTree& operator=(const BinaryTree&);
  BinaryTree& operator=(BinaryTree&&);

  void put(const T& element, const std::vector<bool>& path);
  void print_left_to_right(std::ostream&) const;
  void print_structure(std::ostream&) const;

  std::size_t even_elements_count() const;
  bool contains_only_positive_elements() const;
  void remove_leaves();
  double get_average_all_elements() const;
  std::vector<bool> find(const T& element) const;
  bool is_search_tree() const;

 private:
  void _fast_copy_tree(const Node* fromRoot, Node*& toRoot);
  bool _fast_remove_if_leaf(Node*);
  void _fast_find(Node* root, const T& element, std::vector<bool>& path) const;
  void _fast_is_search_tree(Node* root,
                            const bool less_then_element,
                            const T& prev_root_element,
                            const T* prev_prev_root_element) const;

  template<class Function, class Pre_Function = std::nullptr_t, class Post_Function = std::nullptr_t>
  void _fast_for_each(Node*,
                      Function,
                      Pre_Function pre_func = nullptr,
                      Post_Function post_func = nullptr) const;
};

#include "BinaryTree.inl"
