#pragma once

#include <exception>

class BinaryTreeException : public std::exception {
 private:
  const char* msg;

 public:
  BinaryTreeException(const char* msg = "") : msg(msg) {}

  const char* what() const noexcept override {
    return msg;
  }
};
