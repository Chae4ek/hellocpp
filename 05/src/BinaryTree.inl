#pragma once

#include <iostream>

#include "BinaryTree.h"
#include "BinaryTreeException.h"

template<class T>
BinaryTree<T>::BinaryTree() {}

template<class T>
BinaryTree<T>::~BinaryTree() {
  if (root) delete root;  // Node destructor is invoked
}

template<class T>
BinaryTree<T>::BinaryTree(const BinaryTree<T>& bt) {
  _fast_copy_tree(bt.root, root);
  tree_size = bt.tree_size;
}

template<class T>
BinaryTree<T>::BinaryTree(BinaryTree<T>&& bt) {
  root = std::move(bt.root);
  bt.root = nullptr;
  tree_size = std::move(bt.tree_size);
}

template<class T>
BinaryTree<T>& BinaryTree<T>::operator=(const BinaryTree<T>& bt) {
  if (this == &bt) return *this;
  BinaryTree temp(bt);
  std::swap(root, temp.root);
  std::swap(tree_size, temp.tree_size);
  return *this;
}

template<class T>
BinaryTree<T>& BinaryTree<T>::operator=(BinaryTree<T>&& bt) {
  if (this == &bt) return *this;
  std::swap(root, bt.root);
  std::swap(tree_size, bt.tree_size);
  return *this;
}

template<class T>
void BinaryTree<T>::put(const T& element, const std::vector<bool>& path) {
  if (path.empty()) {
    if (root)
      root->element = element;
    else {
      root = new Node(element);
      ++tree_size;
    }
    return;
  }
  if (!root) throw BinaryTreeException("error: incorrect path");
  auto* curr = root;
  for (std::size_t i = 0; i < path.size() - 1; ++i) {
    if (path[i]) {
      if (!curr->right) throw BinaryTreeException("error: incorrect path");
      curr = curr->right;
    } else {
      if (!curr->left) throw BinaryTreeException("error: incorrect path");
      curr = curr->left;
    }
  }  // now curr is before last one
  if (path.back()) {
    if (curr->right)
      curr->right->element = element;
    else {
      curr->right = new Node(element);
      ++tree_size;
    }
  } else {
    if (curr->left)
      curr->left->element = element;
    else {
      curr->left = new Node(element);
      ++tree_size;
    }
  }
}

template<class T>
void BinaryTree<T>::print_left_to_right(std::ostream& out) const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  _fast_for_each(root, [&](Node* r) { out << r->element << ' '; });
  out << std::endl;
}

template<class T>
void BinaryTree<T>::print_structure(std::ostream& out) const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  std::size_t tabs = 0;
  _fast_for_each(
      root, nullptr,
      [&](Node* r) {  // pre-function
        for (std::size_t i = 0; i < tabs; ++i) out << '\t';
        out << r->element << '\n';
        ++tabs;
      },
      [&](Node*) { --tabs; });  // post-function
  out << std::endl;
}

template<class T>
std::size_t BinaryTree<T>::even_elements_count() const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  std::size_t count = 0;
  _fast_for_each(root, [&](Node* r) {
    if ((r->element & 1) == 0) ++count;
  });
  return count;
}

template<class T>
bool BinaryTree<T>::contains_only_positive_elements() const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  bool only_positive = true;
  try {
    _fast_for_each(root, [&](Node* r) {
      if (r->element <= 0) only_positive = false, throw BinaryTreeException();
    });
  } catch (BinaryTreeException& e) {
    // all right, that is a fast exit
  }
  return only_positive;
}

template<class T>
void BinaryTree<T>::remove_leaves() {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");

  if (root->left || root->right) _fast_remove_if_leaf(root);
}

template<class T>
double BinaryTree<T>::get_average_all_elements() const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  long long sum = 0;
  double average = 0;
  _fast_for_each(root, [&](Node* r) {
    // sum + abs(x) >= sum   <=>   abs(x) >= 0
    // sum - abs(x) <= sum   <=>   -abs(x) <= 0
    if (r->element >= 0) {
      if (static_cast<long long>(sum + r->element) < sum)
        average += static_cast<double>(sum) / tree_size, sum = 0;
    } else {
      if (static_cast<long long>(sum + r->element) > sum)
        average += static_cast<double>(sum) / tree_size, sum = 0;
    }
    sum += r->element;
  });
  return average + static_cast<double>(sum) / tree_size;
}

template<class T>
std::vector<bool> BinaryTree<T>::find(const T& element) const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  std::vector<bool> path;
  try {
    _fast_find(root, element, path);
  } catch (BinaryTreeException& e) {  // throws if found
    return path;
  }
  throw BinaryTreeException("warning: the element was not found");
}

template<class T>
bool BinaryTree<T>::is_search_tree() const {
  if (!root) throw BinaryTreeException("error: tree root is nullptr");
  try {
    if (root->left) _fast_is_search_tree(root->left, true, root->element, nullptr);
    if (root->right) _fast_is_search_tree(root->right, false, root->element, nullptr);
  } catch (BinaryTreeException& e) { return false; }
  return true;
}

template<class T>
void BinaryTree<T>::_fast_copy_tree(const Node* fromRoot, Node*& toRoot) {
  if (!fromRoot || !toRoot) return;
  toRoot = new Node(fromRoot->element);
  _fast_copy_tree(fromRoot->left, toRoot->left);
  _fast_copy_tree(fromRoot->right, toRoot->right);
}

/**
 * @return true if it was a leaf
 */
template<class T>
bool BinaryTree<T>::_fast_remove_if_leaf(Node* root) {
  if (!root) return false;
  bool is_it_leaf = true;
  if (root->left) {
    if (_fast_remove_if_leaf(root->left)) root->left = nullptr;
    is_it_leaf = false;
  }
  if (root->right) {
    if (_fast_remove_if_leaf(root->right)) root->right = nullptr;
    is_it_leaf = false;
  }
  return is_it_leaf ? (delete root, --tree_size, true) : false;
}

/**
 * @throws BinaryTreeException if the element was found
 */
template<class T>
void BinaryTree<T>::_fast_find(Node* root, const T& element, std::vector<bool>& path) const {
  if (!root) return;
  if (element == root->element) throw BinaryTreeException();  // fast exit
  if (root->left) {
    path.push_back(0);
    _fast_find(root->left, element, path);
  }
  if (root->right) {
    path.push_back(1);
    _fast_find(root->right, element, path);
  }
  path.pop_back();
  return;
}

/**
 * @throws BinaryTreeException if the tree is not a binary search tree
 */
template<class T>
void BinaryTree<T>::_fast_is_search_tree(Node* root,
                                         const bool less_then_element,
                                         const T& prev_root_element,
                                         const T* prev_prev_root_element) const {
  if (!root) return;
  if (less_then_element) {
    if (root->element >= prev_root_element ||
        (prev_prev_root_element && root->element <= *prev_prev_root_element))
      throw BinaryTreeException();  // fast exit
  } else {
    if (root->element <= prev_root_element ||
        (prev_prev_root_element && root->element >= *prev_prev_root_element))
      throw BinaryTreeException();  // fast exit
  }
  if (root->left)
    _fast_is_search_tree(root->left, true, root->element,
                         less_then_element ? prev_prev_root_element : &prev_root_element);
  if (root->right)
    _fast_is_search_tree(root->right, false, root->element,
                         less_then_element ? &prev_root_element : prev_prev_root_element);
}

template<class T>
template<class Function, class Pre_Function, class Post_Function>
void BinaryTree<T>::_fast_for_each(Node* root,
                                   Function func,
                                   Pre_Function pre_func,
                                   Post_Function post_func) const {
  if (!root) return;
  if constexpr (!std::is_same_v<Pre_Function, std::nullptr_t>) pre_func(root);
  if (root->left) _fast_for_each(root->left, func, pre_func, post_func);
  if constexpr (!std::is_same_v<Function, std::nullptr_t>) func(root);
  if (root->right) _fast_for_each(root->right, func, pre_func, post_func);
  if constexpr (!std::is_same_v<Post_Function, std::nullptr_t>) post_func(root);
}
