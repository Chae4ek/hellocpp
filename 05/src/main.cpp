#include <iostream>

#include "BinaryTree.h"

void demo() {
  BinaryTree<short> bt;

  bt.put(10000, {});
  bt.put(20000, {0});
  bt.put(70, {1});
  bt.put(30000, {1, 0});
  bt.put(4000, {0, 0});

  bt.print_structure(std::cout);

  std::cout << "even elements count = " << bt.even_elements_count() << std::endl;
  std::cout << "is search tree = " << bt.is_search_tree() << std::endl;

  std::cout << "-----------------------------------" << std::endl;
  auto a = std::move(bt);

  a.print_structure(std::cout);
  std::cout << "-----------------------------------" << std::endl;
  a.remove_leaves();
  a.print_structure(std::cout);

  std::cout << "even elements count = " << a.even_elements_count();

  std::cout.precision(12);
  std::cout << std::endl << "only positive = " << a.contains_only_positive_elements() << std::endl;
  std::cout << "average = " << a.get_average_all_elements() << std::endl;
  std::cout << "-----------------------------------" << std::endl;
  a.put(-8, {0});
  a.print_structure(std::cout);
  std::cout << "only positive = " << a.contains_only_positive_elements();
  std::cout << std::endl << "average = " << a.get_average_all_elements() << std::endl;
  std::cout << "even elements count = " << a.even_elements_count() << std::endl;

  std::cout << "70 is here: ";
  auto found = a.find(70);
  for (auto i : found) { std::cout << i << ' '; }

  std::cout << std::endl;
  a.remove_leaves();
  a.print_structure(std::cout);

  a.remove_leaves();
  try {
    a.print_structure(std::cout);
  } catch (BinaryTreeException& e) { std::cout << e.what() << std::endl; }

  a.put(1, {});
  a.put(2, {0});
  a.print_structure(std::cout);
  a.remove_leaves();
  try {
    a.print_structure(std::cout);
  } catch (BinaryTreeException& e) { std::cout << e.what() << std::endl; }

  std::cout << "-----------------------------------" << std::endl;

  BinaryTree<int> b;

  b.put(50, {});
  b.put(20, {0});
  b.put(70, {1});
  b.put(10, {0, 0});
  b.put(33, {0, 1});
  b.put(60, {1, 0});
  b.put(100, {1, 1});

  b.print_left_to_right(std::cout);
  b.print_structure(std::cout);

  std::cout << "33 is here: ";
  found = b.find(33);
  for (auto i : found) { std::cout << i << ' '; }

  std::cout << std::endl << "60 is here: ";
  found = b.find(60);
  for (auto i : found) { std::cout << i << ' '; }

  std::cout << std::endl << "is search tree = " << b.is_search_tree();
}

int main() {
  demo();
  return 0;
}
