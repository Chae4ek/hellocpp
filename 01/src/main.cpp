#include <iostream>

#include "DynamicIntArray.h"

void demo1() {
  std::ios_base::sync_with_stdio(false);

  DynamicIntArray a1(10);
  try {
    a1[2];
  } catch (std::out_of_range& e) {
    std::cout << e.what() << std::endl;
  }

  DynamicIntArray a(5, 0), b(10, 0);
  a[2] = 2;
  b = a;
  std::cout << (a[2] == b[2]) << std::endl;
  std::cout << (a == b) << std::endl;
  a[4] = 1;
  std::cout << (a == b) << std::endl;

  a.resize(11);

  std::cout << a << std::endl;
  std::cout << b << std::endl;

  std::cout << (a + b) << std::endl;
  std::cout << (a > b) << std::endl;
}

void demo_last() {
  DynamicIntArray a(10, 0);
  a[2] = 2;
  a[5] = 1;

  DynamicIntArray c(a);
  std::cout << a << std::endl;
  std::cout << c << std::endl;

  DynamicIntArray d(std::move(c));
  std::cout << d << std::endl;

  DynamicIntArray d1 = std::move(d);
  std::cout << d1 << std::endl;

  d1 = d1;
  std::cout << std::endl << d1 << std::endl;

  d1 = std::move(d1);
  std::cout << d1 << std::endl;

  try {
    std::cout << c;  // does not caught
  } catch (...) {
  }
}

int main() {
  demo1();
  demo_last();
}
