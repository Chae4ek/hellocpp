#include "DynamicIntArray.h"

DynamicIntArray::DynamicIntArray(std::size_t capacity) {
  __capacity = capacity;
  __array = new int[capacity];
}

DynamicIntArray::DynamicIntArray(std::size_t size, int n) {
  __size = __capacity = size;
  __array = new int[__capacity];
  for (std::size_t i = 0; i < size; ++i) __array[i] = n;
}

DynamicIntArray::DynamicIntArray(const DynamicIntArray& d) {
  __size = d.__size;
  __capacity = __size;
  __array = new int[__size];
  for (std::size_t i = 0; i < __size; ++i) __array[i] = d.__array[i];
}

DynamicIntArray::DynamicIntArray(DynamicIntArray&& d) {
  __array = std::move(d.__array);
  d.__array = nullptr;
  __size = std::move(d.__size);
  __capacity = std::move(d.__capacity);
}

DynamicIntArray::~DynamicIntArray() {
  delete[] __array;
}

inline constexpr std::size_t DynamicIntArray::get_size() const {
  return __size;
}
inline constexpr std::size_t DynamicIntArray::get_capacity() const {
  return __capacity;
}

void DynamicIntArray::resize(std::size_t new_size) {
  if (new_size > __capacity) {
    __capacity = new_size;
    int* new_array = new int[new_size];
    for (std::size_t i = 0; i < __size; ++i) new_array[i] = std::move(__array[i]);
    delete[] __array;
    __array = new_array;
  }
  for (std::size_t i = __size; i < new_size; ++i) __array[i] = 0;
  __size = new_size;
}

void DynamicIntArray::reserve(std::size_t n) {
  __capacity += n;
  int* new_array = new int[__capacity];
  for (std::size_t i = 0; i < __size; ++i) new_array[i] = std::move(__array[i]);
  delete[] __array;
  __array = new_array;
}

void DynamicIntArray::push_back(int x) {
  if (__size == __capacity) reserve(5);
  __array[__size] = x;
  ++__size;
}

void DynamicIntArray::pop_back() {
  if (__size == 0) throw std::out_of_range("error: array is empty");
  --__size;
}

inline constexpr int& DynamicIntArray::operator[](std::size_t i) const {
  if (i >= __size) throw std::out_of_range("error: out of array bounds");
  return __array[i];
}
constexpr DynamicIntArray& DynamicIntArray::operator=(const DynamicIntArray& d) {
  if (this == &d) return *this;
  __size = d.__size;
  if (__size > __capacity) {
    __capacity = __size;
    delete[] __array;
    __array = new int[__size];
  }
  for (std::size_t i = 0; i < __size; ++i) __array[i] = d.__array[i];
  return *this;
}
DynamicIntArray& DynamicIntArray::operator=(DynamicIntArray&& d) {
  if (this == &d) return *this;
  delete[] __array;
  __array = std::move(d.__array);
  d.__array = nullptr;
  __size = std::move(d.__size);
  __capacity = std::move(d.__capacity);
  return *this;
}
constexpr bool DynamicIntArray::operator==(const DynamicIntArray& d) const {
  if (this == &d || this->__array == d.__array) return true;
  if (this->__size != d.__size) throw std::length_error("error: sizes are not equal");
  for (std::size_t i = 0; i < __size; ++i)
    if (__array[i] != d.__array[i]) return false;
  return true;
}
inline constexpr bool DynamicIntArray::operator!=(const DynamicIntArray& d) const {
  return !(*this == d);
}
DynamicIntArray DynamicIntArray::operator+(const DynamicIntArray& d) const {
  DynamicIntArray new_dynamic_array(__size + d.__size);
  for (std::size_t i = 0; i < __size; ++i) new_dynamic_array.__array[i] = __array[i];
  for (std::size_t i = 0; i < d.__size; ++i) new_dynamic_array.__array[__size + i] = d.__array[i];
  new_dynamic_array.__size = new_dynamic_array.__capacity;
  return new_dynamic_array;
}

constexpr int DynamicIntArray::__this_less_than(const DynamicIntArray& d) const {
  std::size_t min_size = __size < d.__size ? __size : d.__size;
  for (std::size_t i = 0; i < min_size; ++i)
    if (__array[i] < d.__array[i])
      return 1;
    else if (__array[i] > d.__array[i])
      return -1;
  if (__size < d.__size) return 1;
  if (__size > d.__size) return -1;
  return 0;
}

inline constexpr bool DynamicIntArray::operator<(const DynamicIntArray& d) const {
  return 0 < __this_less_than(d);
}
inline constexpr bool DynamicIntArray::operator>(const DynamicIntArray& d) const {
  return 0 > __this_less_than(d);
}
inline constexpr bool DynamicIntArray::operator<=(const DynamicIntArray& d) const {
  return 0 <= __this_less_than(d);
}
inline constexpr bool DynamicIntArray::operator>=(const DynamicIntArray& d) const {
  return 0 >= __this_less_than(d);
}

std::ostream& operator<<(std::ostream& out, const DynamicIntArray& d) {
  out << "size=" << d.__size << " capacity=" << d.__capacity << " array=";
  for (std::size_t i = 0; i < d.__size; ++i) out << " " << d[i];
  return out;
}
std::istream& operator>>(std::istream& in, DynamicIntArray& d) {
  std::size_t s;
  in >> s;
  if (s > d.__capacity) throw std::out_of_range("error: size more than capacity");
  d.__size = s;
  for (std::size_t i = 0; i < s; ++i) in >> d.__array[i];
  return in;
}
