#pragma once

#include <iostream>

class DynamicIntArray {
 private:
  int* __array = nullptr;
  std::size_t __size = 0;
  std::size_t __capacity = 0;

 public:
  DynamicIntArray(std::size_t capacity = 10);
  DynamicIntArray(std::size_t size, int n);
  DynamicIntArray(const DynamicIntArray&);
  DynamicIntArray(DynamicIntArray&&);
  ~DynamicIntArray();

  inline constexpr std::size_t get_size() const;
  inline constexpr std::size_t get_capacity() const;

  void resize(std::size_t new_size);
  void reserve(std::size_t n);
  void push_back(int x);
  void pop_back();

  inline constexpr int& operator[](std::size_t i) const;
  constexpr DynamicIntArray& operator=(const DynamicIntArray& d);
  DynamicIntArray& operator=(DynamicIntArray&& d);
  constexpr bool operator==(const DynamicIntArray& d) const;
  inline constexpr bool operator!=(const DynamicIntArray& d) const;
  DynamicIntArray operator+(const DynamicIntArray& d) const;

 private:
  constexpr int __this_less_than(const DynamicIntArray& d) const;

 public:
  inline constexpr bool operator<(const DynamicIntArray& d) const;
  inline constexpr bool operator>(const DynamicIntArray& d) const;
  inline constexpr bool operator<=(const DynamicIntArray& d) const;
  inline constexpr bool operator>=(const DynamicIntArray& d) const;

  friend std::ostream& operator<<(std::ostream& out, const DynamicIntArray& d);
  friend std::istream& operator>>(std::istream& in, DynamicIntArray& d);
};

#include "DynamicIntArray.inl"
