#include <iostream>

#include "Container.h"
#include "Functions.h"
using namespace task15;

void demo_box() {
  Box box(10, 10, 10, 5, 100), box2(10, 10, 10, 5, 100), box3(1, 1, 1, 1, 1);
  std::cout << box << std::endl;
  std::cout << (box == box2) << std::endl;
  std::cout << (box == box3) << std::endl;

  std::cout << std::endl << box.get_length() << std::endl;

  Box boxes[] = {Box(1, 1, 1, 1, 1), Box(10, 10, 10, 1, 1), Box(8, 6, 5, 1, 1)};
  std::cout << std::endl << can_insert(boxes, 3) << std::endl;

  Box boxes2[] = {Box(10, 1, 1, 1, 1), Box(10, 10, 10, 1, 1), Box(8, 6, 5, 1, 1)};
  std::cout << can_insert(boxes2, 3) << std::endl;

  std::cin >> box;
  std::cout << box;
}

void demo_container() {
  Container c(10, 10, 10, 20);

  Box b(1, 2, 3, 4, 5), b2(5, 4, 3, 2, 1);

  c.push(b, 0);
  c.push(b2, 0);

  std::cout << c.get_box(0) << std::endl << c.get_box(1) << std::endl;

  std::cout << c.get_sum_weight() << " " << c.get_sum_value() << std::endl;

  c.pop(0);

  c[0].length = 10000;

  std::cout << c.get_box_count() << " " << c[0] << std::endl;
}

int main() {
  std::ios_base::sync_with_stdio(false);

  demo_box();
  demo_container();
}
