#include "Functions.h"

int sum_all(Box boxes[], int size) {
  int sum = 0;
  for (int i = 0; i < size; ++i) sum += boxes[i].value;
  return sum;
}

bool check_sums(Box boxes[], int size, int max) {
  for (int i = 0; i < size; ++i)
    if (boxes[i].length > max || boxes[i].width > max || boxes[i].height > max) return false;
  return true;
}

double find_max_weight(Box boxes[], int size, long long maxV) {
  double max = -1;
  for (int i = 0; i < size; ++i)
    if (boxes[i].length * boxes[i].width * boxes[i].height <= maxV) max = boxes[i].weight;
  return max;
}

inline constexpr bool box_inserts(Box &box, Box &in) {
  return box.length < in.length && box.width < in.width && box.height < in.height;
}
bool can_insert(Box boxes[], int size) {
  Box *max_box = nullptr;
  for (int i = 0; i < size; ++i) {
    if (max_box != nullptr && box_inserts(boxes[i], *max_box)) continue;

    bool not_inserted = true;
    for (int j = i + 1; j < size; ++j)
      if (box_inserts(boxes[i], boxes[j])) {
        not_inserted = false;
        break;
      }

    if (not_inserted) {
      if (max_box != nullptr) return false;
      max_box = &boxes[i];
    }
  }
  return true;
}
