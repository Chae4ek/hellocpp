#pragma once

#include <vector>

#include "Box.h"
#include "MaxWeightException.h"

namespace task15 {

class Container {
 private:
  std::vector<Box> boxes;

  int length;
  int width;
  int height;
  double max_weight;

 public:
  Container(int length, int width, int height, double max_weight)
      : length(length), width(width), height(height), max_weight(max_weight) {}

  friend inline std::ostream& operator<<(std::ostream& out, const Container& c) {
    out << "length=" << c.length << ", width=" << c.width << ", height=" << c.height << ", max weight=" << c.max_weight;
    return out;
  }
  friend inline std::istream& operator>>(std::istream& in, Container& c) {
    in >> c.length >> c.width >> c.height >> c.max_weight;
    return in;
  }

  /**
   * Добавляет коробку и возвращает ее индекс, если суммарный вес позволяет, иначе кидает MaxWeightException
   */
  inline int push(Box& b, int index) {
    if (get_sum_weight() + b.weight > max_weight) throw MaxWeightException("Max weight is reached");
    boxes.insert(boxes.begin() + index, b);
    return index;
  }
  inline void pop(int index) {
    boxes.erase(boxes.begin() + index);
  }

  inline int get_box_count() {
    return boxes.size();
  }
  double get_sum_weight() {
    double sum = 0;
    for (auto& i : boxes) sum += i.weight;
    return sum;
  }
  int get_sum_value() {
    double sum = 0;
    for (auto& i : boxes) sum += i.value;
    return sum;
  }
  inline Box& get_box(int index) {
    return boxes.at(index);
  }

  inline Box& operator[](int i) {
    return boxes[i];
  }
};

}
