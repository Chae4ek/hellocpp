#pragma once

#include <math.h>

#include <iostream>

namespace task15 {

class Box {
 public:
  int length;
  int width;
  int height;
  double weight;
  int value;

  Box(int length, int width, int height, double weight, int value)
      : length(length), width(width), height(height), weight(weight), value(value) {}

  inline constexpr bool operator==(const Box& right) const {
    return this->length == right.length && this->width == right.width && this->height == right.height &&
           this->value == right.value && abs(this->weight - right.weight) < 1e-10;
  }

  friend inline std::ostream& operator<<(std::ostream& out, const Box& b) {
    out << "length=" << b.length << ", width=" << b.width << ", height=" << b.height << ", weight=" << b.weight
        << ", value=" << b.value;
    return out;
  }
  friend inline std::istream& operator>>(std::istream& in, Box& b) {
    in >> b.length >> b.width >> b.height >> b.weight >> b.value;
    return in;
  }

  inline constexpr int get_length() const {
    return length;
  }
  inline constexpr int get_width() const {
    return width;
  }
  inline constexpr int get_height() const {
    return height;
  }
  inline constexpr double get_weight() const {
    return weight;
  }
  inline constexpr int get_value() const {
    return value;
  }

  inline constexpr void set_length(int length) {
    this->length = length;
  }
  inline constexpr void set_width(int width) {
    this->width = width;
  }
  inline constexpr void set_height(int height) {
    this->height = height;
  }
  inline constexpr void set_weight(double weight) {
    this->weight = weight;
  }
  inline constexpr void set_value(int value) {
    this->value = value;
  }
};

}
