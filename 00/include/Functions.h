#pragma once

#include "Box.h"
using namespace task15;

/**
 * Вычисляет суммарную стоимость
 */
int sum_all(Box[], int size);
/**
 * Проверяет, что сумма длины, ширины и высоты всех не превосходит заданного значения
 */
bool check_sums(Box[], int size, int max);
/**
 * Максимальный вес коробок, объем которых не больше параметра
 * @return -1 если значение не найдено
 */
double find_max_weight(Box[], int size, long long maxV);
/**
 * Проверяет, можно ли вложить коробки друг в друга
 */
bool can_insert(Box[], int size);
