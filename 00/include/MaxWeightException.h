#pragma once

#include <string>

class MaxWeightException {
 private:
  std::string msg_error;

 public:
  MaxWeightException(std::string msg_error = "") : msg_error(msg_error) {}

  const char* get_msg_error() {
    return msg_error.c_str();
  }
};
