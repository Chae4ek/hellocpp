all:

clean:
	rm -rf **/.ccls-cache/ && \
	rm -rf **/.vscode/ && \
	rm -rf **/bin/ && \
	rm -rf **/build/ && \
	rm -rf **/.ccls && \
	rm -rf **/compile_commands.json
