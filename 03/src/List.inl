#pragma once

#include <stdexcept>
#include <utility>

#include "List.h"

template<class T>
List<T>::List() {
  __curr = __zero = new Node<T>(0);
  __curr->prev = __curr->next = __curr;
}

template<class T>
List<T>::List(const List<T>& list) {
  __curr = __zero = new Node<T>(0);
  __curr->prev = __curr->next = __curr;

  Node<T>* cur = list.__zero->next;
  while (cur != list.__zero) {
    __curr = __curr->next = __curr->next->prev = new Node<T>(cur->value, __curr, __curr->next);
    cur = cur->next;
  }
  __curr = __zero;
  __size = list.__size;
}

template<class T>
List<T>::List(List<T>&& list) {
  __zero = std::move(list.__zero);
  list.__zero = nullptr;
  __curr = std::move(list.__curr);
  list.__curr = nullptr;
  __size = std::move(list.__size);
  list.__size = nullptr;
}

template<class T>
List<T>::~List() {
  __zero->prev->next = nullptr;
  do {
    Node<T>* next = __zero->next;
    delete __zero;
    __zero = next;
  } while (__zero);
}

template<class T>
void List<T>::push_current(T element) {
  __curr = __curr->prev = __curr->prev->next = new Node<T>(element, __curr->prev, __curr);
  ++__size;
}

template<class T>
void List<T>::pop_current() {
  if (__curr == __zero) throw new std::out_of_range("error: current out of list");
  __curr->next->prev = __curr->prev;
  __curr->prev->next = __curr->next;
  Node<T>* temp = __curr;
  __curr = __curr->prev;
  delete temp;
  --__size;
}

template<class T>
typename List<T>::iterator_ptr List<T>::find(T element) const {
  Node<T>* cur = __zero->next;
  while (cur != __zero) {
    if (element == cur->value) return cur;
    cur = cur->next;
  }
  return nullptr;
}

template<class T>
void List<T>::clear() {
  __curr = __zero->next;
  do {
    Node<T>* next = __curr->next;
    delete __curr;
    __curr = next;
  } while (__curr != __zero);
  __curr->prev = __curr->next = __curr;
  __size = 0;
}

template<class T>
inline constexpr bool List<T>::is_empty() const {
  return __size == 0;
}

template<class T>
inline constexpr std::size_t List<T>::size() const {
  return __size;
}

template<class T>
inline constexpr typename List<T>::iterator_ptr List<T>::begin() const {
  return __zero == __zero->next ? nullptr : __zero->next;
}

template<class T>
inline /*constexpr*/ void List<T>::start() {
  __curr = __zero->next;
}

template<class T>
inline /*constexpr*/ T List<T>::get_current() const {
  return __curr->value;
}

template<class T>
inline /*constexpr*/ void List<T>::next() {
  __curr = __curr->next;
}

template<class T>
inline /*constexpr*/ bool List<T>::is_end() const {  // constexpr is unstable (Bug 91846)
  return __curr == __zero;
}

template<class T>
List<T>& List<T>::operator=(List<T>& list) {
  if (*this == list) return *this;
  delete this;
  return this(list);
}
