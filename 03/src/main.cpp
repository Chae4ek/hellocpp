#include <iostream>

#include "List.h"

void demo() {
  List<int> a1;

  a1.push_current(1);
  a1.push_current(2);
  a1.push_current(3);
  a1.push_current(4);

  List<int> a2(a1);
  a2.start();
  a2.pop_current();

  std::cout << a1.size() << ' ' << a2.size() << std::endl;

  a1.start();
  while (!a1.is_end()) {
    std::cout << a1.get_current() << std::endl;
    a1.next();
  }

  std::cout << std::endl;

  a2.start();
  while (!a2.is_end()) {
    std::cout << a2.get_current() << std::endl;
    a2.next();
  }
}

int main() {
  demo();
  return 0;
}
