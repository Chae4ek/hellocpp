#pragma once

#include "IIterator.h"
#include "IList.h"

template<class T>
class List : public IList<T>, public IIterator<T> {
 public:
  typedef typename IList<T>::iterator_ptr iterator_ptr;

 private:
  template<class K>
  struct Node : public IList<K>::iterator {
   public:
    K value;
    Node* prev = nullptr;
    Node* next = nullptr;

    explicit Node(K value, Node* prev = nullptr, Node* next = nullptr) : value(value), prev(prev), next(next) {}
  };

  Node<T>* __zero = nullptr;
  Node<T>* __curr = nullptr;

  std::size_t __size = 0;

 public:
  List();
  List(const List<T>&);
  List(List<T>&&);
  ~List();

  void push_current(T) override;
  void pop_current() override;
  iterator_ptr find(T) const override;
  void clear() override;
  inline constexpr bool is_empty() const override;
  inline constexpr std::size_t size() const override;
  inline constexpr iterator_ptr begin() const override;

  inline /*constexpr*/ void start() override;
  inline /*constexpr*/ T get_current() const override;
  inline /*constexpr*/ void next() override;
  inline /*constexpr*/ bool is_end() const override;  // constexpr is unstable (Bug 91846)

  List<T>& operator=(List<T>&);
};

#include "List.inl"
