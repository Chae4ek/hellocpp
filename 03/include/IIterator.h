#pragma once

template<class T>
class IIterator {
 public:
  virtual void start() = 0;
  virtual T get_current() const = 0;
  virtual void next() = 0;
  virtual bool is_end() const = 0;
};
