#pragma once

template<class T>
class IList {
 public:
  struct iterator {};
  typedef iterator* iterator_ptr;

  virtual void push_current(T) = 0;
  virtual void pop_current() = 0;
  virtual iterator_ptr find(T) const = 0;
  virtual void clear() = 0;
  virtual bool is_empty() const = 0;
  virtual std::size_t size() const = 0;
  virtual iterator_ptr begin() const = 0;
};
