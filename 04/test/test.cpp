#include <iostream>

template<class K>
long hash_bad(const K& key) {
  return reinterpret_cast<long>(key);
}

template<class K>
inline constexpr const auto* __get_declaration_ptr(const K* key) noexcept {
  if constexpr (std::is_same_v<K, std::remove_pointer_t<K>>)
    return static_cast<const K*>(key);
  else
    return __get_declaration_ptr(*key);
}

template<class K>
inline constexpr auto hash_good(const K& key) {
  return std::_Fnv_hash_impl::hash(*__get_declaration_ptr(&key));
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////////////--------- TESTS ---------////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
void demo1() {
  int a = 123;
  int* p1 = &a;
  int** p2 = &p1;
  int*** p3 = &p2;

  int& a1 = a;
  int&& a2 = 123;
  int* a3 = &a2;

  // std::cout << hash_bad(a) << std::endl;  // error
  std::cout << hash_bad(&a) << " =?= " << (long) &a << " == " << (hash_bad(&a) == (long) &a)
            << std::endl;  // ok
  // std::cout << hash_bad(a1) << std::endl;  // error
  std::cout << hash_bad(&a1) << " =?= " << (long) &a << " == " << (hash_bad(&a1) == (long) &a)
            << std::endl;  // ok
  std::cout << hash_bad(p3) << " =?= " << (long) &a << " == " << (hash_bad(p3) == (long) &a)
            << std::endl;  // ok

  // std::cout << hash_bad(a2) << std::endl;  // error
  std::cout << hash_bad(&a2) << " =?= " << (long) &a2 << " == " << (hash_bad(&a2) == (long) &a2)
            << std::endl;  // ok
  std::cout << hash_bad(a3) << " =?= " << (long) &a2 << " == " << (hash_bad(a3) == (long) &a2)
            << std::endl;  // ok
  std::cout << hash_bad(&a3) << " =?= " << (long) &a2 << " == " << (hash_bad(&a3) == (long) &a2)
            << std::endl;  // ok
  // std::cout << hash_bad(*a3) << std::endl;  // error

  int b = 4;
  p1 = &b;

  std::cout << hash_bad(p1) << " =?= " << (long) &b << " == " << (hash_bad(p1) == (long) &b)
            << std::endl;  // ok
  std::cout << hash_bad(&p1) << " =?= " << (long) &b << " == " << (hash_bad(&p1) == (long) &b)
            << std::endl;  // ok
  // std::cout << hash_bad(*p1) << std::endl;  // error
  std::cout << hash_bad(p3) << " =?= " << (long) &b << " == " << (hash_bad(p3) == (long) &b)
            << std::endl;  // ok

  long _a = hash_bad(123L);                                                  // ok
  long _b = hash_bad(123L);                                                  // ok
  long _c = hash_bad(1223L);                                                 // ok
  std::cout << _c << std::endl;                                              // ok
  std::cout << _a << " == " << _b << " == " << hash_bad(123L) << std::endl;  // ok
}

void demo2() {
  int a = 123;
  int* p1 = &a;
  int** p2 = &p1;
  int*** p3 = &p2;

  int& a1 = a;
  int&& a2 = 123;
  int* a3 = &a2;

  std::cout << hash_good(a) << " == " << (hash_good(a) == std::_Fnv_hash_impl::hash(a)) << std::endl;    // ok
  std::cout << hash_good(&a) << " == " << (hash_good(&a) == std::_Fnv_hash_impl::hash(a)) << std::endl;  // ok
  std::cout << hash_good(a1) << " == " << (hash_good(a1) == std::_Fnv_hash_impl::hash(a)) << std::endl;  // ok
  std::cout << hash_good(&a1) << " == " << (hash_good(&a1) == std::_Fnv_hash_impl::hash(a))
            << std::endl;                                                                                // ok
  std::cout << hash_good(p3) << " == " << (hash_good(p3) == std::_Fnv_hash_impl::hash(a)) << std::endl;  // ok

  std::cout << hash_good(a2) << " == " << (hash_good(a2) == std::_Fnv_hash_impl::hash(a2))
            << std::endl;  // ok
  std::cout << hash_good(&a2) << " == " << (hash_good(&a2) == std::_Fnv_hash_impl::hash(a2))
            << std::endl;  // ok
  std::cout << hash_good(a3) << " == " << (hash_good(a3) == std::_Fnv_hash_impl::hash(a2))
            << std::endl;  // ok
  std::cout << hash_good(&a3) << " == " << (hash_good(&a3) == std::_Fnv_hash_impl::hash(a2))
            << std::endl;  // ok
  std::cout << hash_good(*a3) << " == " << (hash_good(*a3) == std::_Fnv_hash_impl::hash(a2))
            << std::endl;  // ok

  int b = 4;
  p1 = &b;

  std::cout << hash_good(p1) << " == " << (hash_good(p1) == std::_Fnv_hash_impl::hash(b)) << std::endl;  // ok
  std::cout << hash_good(&p1) << " == " << (hash_good(&p1) == std::_Fnv_hash_impl::hash(b))
            << std::endl;  // ok
  std::cout << hash_good(*p1) << " == " << (hash_good(*p1) == std::_Fnv_hash_impl::hash(b))
            << std::endl;                                                                                // ok
  std::cout << hash_good(p3) << " == " << (hash_good(p3) == std::_Fnv_hash_impl::hash(b)) << std::endl;  // ok

  auto _a = hash_good(123L);                                                  // ok
  auto _b = hash_good(123L);                                                  // ok
  auto _c = hash_good(1223L);                                                 // ok
  std::cout << _c << std::endl;                                               // ok
  std::cout << _a << " == " << _b << " == " << hash_good(123L) << std::endl;  // ok
}

class A {
 public:
  A() {}
  virtual void f() {
    std::cout << "A";
  }
};
class B : public A {
 public:
  B() {}
  void f() override {
    std::cout << "B";
  }
};
class C : public B {
 public:
  int i;
  C(int s) : i(s) {}
  void f() override {
    std::cout << "C";
  }
};

int main() {
  demo1();
  std::cout << std::endl;
  demo2();
  C* _c = new C(2);
  B* b = new C(1);
  A* a = b;
  A** c = &a;
  std::cout << std::endl
            << hash_good(a) << ' ' << hash_good(b) << ' ' << hash_good(c) << " __ \n"
            << hash_good(_c);
  return 0;
}
