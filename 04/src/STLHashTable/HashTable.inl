#pragma once

#include <algorithm>
#include <execution>

#include "STLHashTable/HashTable.h"

namespace stl_ht_impl {

template<class K, class V, class H>
HashTable<K, V, H>::HashTable(std::size_t size) : __table(size, std::list<std::pair<const K, V>>()) {}

template<class K, class V, class H>
void HashTable<K, V, H>::put(const K& key, const V& value) {
  std::size_t hash = H::hash(key) % __table.size();

  auto it = std::find_if(std::execution::par_unseq, __table[hash].begin(), __table[hash].end(),
                         [&](auto& p) { return p.first == key && p.second == value; });

  if (it != __table[hash].end())
    it->second = value;
  else
    __table[hash].emplace_back(key, value);
}

template<class K, class V, class H>
void HashTable<K, V, H>::remove(iterator& it) {
  auto old = it;
  --old;
  std::size_t index = H::hash(it->first) % __table.size();
  for (auto i = __table[index].begin(); i != __table[index].end(); ++i)
    if (i->first == it->first && i->second == it->second) {
      __table[index].erase(i);
      break;
    }
  it = old;
}

template<class K, class V, class H>
typename HashTable<K, V, H>::iterator HashTable<K, V, H>::find(const V& value) const {
  return std::find_if(std::execution::par_unseq, begin(), end(), [&](auto& p) { return p.second == value; });
}

template<class K, class V, class H>
void HashTable<K, V, H>::clear() {
  for (auto& i : __table) i.clear();
}

template<class K, class V, class H>
bool HashTable<K, V, H>::is_empty() const {
  for (auto& i : __table)
    if (i.size() != 0) return false;
  return true;
}

template<class K, class V, class H>
typename HashTable<K, V, H>::iterator HashTable<K, V, H>::begin() const {
  return ++iterator(__table, static_cast<std::size_t>(-1), __table[0].begin());
}

template<class K, class V, class H>
typename HashTable<K, V, H>::iterator HashTable<K, V, H>::end() const {
  return iterator(__table, __table.size(), __table[__table.size() - 1].end());
}

}
