#pragma once

#include "STLHashTable/HashTableIterator.h"

namespace stl_ht_impl {

template<class K, class V>
HashTableIterator<K, V>::HashTableIterator(const HashTableIterator& h)
    : __table(h.__table), __index(h.__index), __bucket_index(h.__bucket_index) {}

template<class K, class V>
HashTableIterator<K, V>::HashTableIterator(const T& table,
                                           std::size_t index,
                                           typename U::const_iterator bucket_index)
    : __table(&table), __index(index), __bucket_index(bucket_index) {}

template<class K, class V>
inline constexpr bool HashTableIterator<K, V>::operator==(const HashTableIterator& r) const {
  return __table == r.__table && __index == r.__index && __bucket_index == r.__bucket_index;
}

template<class K, class V>
inline constexpr bool HashTableIterator<K, V>::operator!=(const HashTableIterator& r) const {
  return !(*this == r);
}

template<class K, class V>
HashTableIterator<K, V>& HashTableIterator<K, V>::operator++() {
  if (__index == __table->size()) return *this;
  if (__index == static_cast<std::size_t>(-1))
    __index = 0;
  else
    ++__bucket_index;

  if (__bucket_index == (*__table)[__index].end()) {
    ++__index;
    while (__index < __table->size() && (*__table)[__index].empty()) ++__index;
    __bucket_index = __index == __table->size() ? (*__table)[__index - 1].end() : (*__table)[__index].begin();
  }
  return *this;
}

template<class K, class V>
HashTableIterator<K, V>& HashTableIterator<K, V>::operator--() {
  if (__index == static_cast<std::size_t>(-1)) return *this;
  if (__index == __table->size() || __bucket_index == (*__table)[__index].begin()) {
    --__index;
    while (__index != static_cast<std::size_t>(-1) && (*__table)[__index].empty()) --__index;
    if (__index == static_cast<std::size_t>(-1))
      __bucket_index = (*__table)[0].begin();
    else {
      __bucket_index = (*__table)[__index].end();
      --__bucket_index;
    }
  } else
    --__bucket_index;
  return *this;
}

template<class K, class V>
inline constexpr typename HashTableIterator<K, V>::P& HashTableIterator<K, V>::operator*() const {
  return const_cast<P&>(*__bucket_index);
}

template<class K, class V>
inline constexpr typename HashTableIterator<K, V>::P* HashTableIterator<K, V>::operator->() const {
  return &const_cast<P&>(*__bucket_index);
}

template<class K, class V>
inline constexpr HashTableIterator<K, V>& HashTableIterator<K, V>::operator=(const HashTableIterator& r) {
  __table = r.__table;
  __index = r.__index;
  __bucket_index = r.__bucket_index;
  return *this;
}

}
