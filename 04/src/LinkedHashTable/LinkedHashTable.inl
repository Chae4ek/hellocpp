#pragma once

#include <algorithm>

#include "LinkedHashTable/LinkedHashTable.h"

namespace std_lht_impl {

template<class K, class V, class H>
LinkedHashTable<K, V, H>::LinkedHashTable(std::size_t size) : __size(size) {
  __table = new Node<const K, V>*[size];
  for (std::size_t i = 0; i < size; ++i) __table[i] = nullptr;
}

template<class K, class V, class H>
LinkedHashTable<K, V, H>::~LinkedHashTable() {
  clear();
}

template<class K, class V, class H>
void LinkedHashTable<K, V, H>::put(const K& key, const V& value) {
  std::size_t hash = H::hash(key) % __size;

  auto* cur = __table[hash];
  Node<const K, V>* prev = nullptr;
  while (cur) {
    prev = cur;
    if (cur->pair.first == key && cur->pair.second == value) break;
    cur = cur->bucket_next;
  }

  if (cur)
    cur->pair.second = value;
  else {
    __last =
        new Node<const K, V>(key, value, prev, nullptr, __last, nullptr);  // Node's constructor is invoked
    if (!__table[hash]) __table[hash] = __last;
    if (!__first) __first = __last;
  }
}

template<class K, class V, class H>
void LinkedHashTable<K, V, H>::remove(iterator& it) {
  auto old = it;
  --old;
  std::size_t index = H::hash(it->first) % __size;
  auto* cur = __table[index];
  while (cur) {
    if (cur->pair.first == it->first && cur->pair.second == it->second) {
      if (cur == __first) __first = __first->global_next;
      if (cur == __last) __last = __last->global_prev;
      if (!cur->bucket_prev) __table[index] = cur->bucket_next;
      delete cur;  // Node's destructor is invoked
      break;
    }
    cur = cur->bucket_next;
  }
  it = old;
}

template<class K, class V, class H>
typename LinkedHashTable<K, V, H>::iterator LinkedHashTable<K, V, H>::find(const V& value) const {
  return std::find_if(begin(), end(), [&](auto& p) { return p.second == value; });
}

template<class K, class V, class H>
void LinkedHashTable<K, V, H>::clear() {
  for (std::size_t i = 0; i < __size; ++i) {
    auto* cur = __table[i];
    while (cur) {
      auto* next = cur->bucket_next;
      delete cur;
      cur = next;
    }
    __table[i] = nullptr;
  }
  __first = __last = nullptr;
}

template<class K, class V, class H>
bool LinkedHashTable<K, V, H>::is_empty() const {
  return !__first;
}

template<class K, class V, class H>
typename LinkedHashTable<K, V, H>::iterator LinkedHashTable<K, V, H>::begin() const {
  return iterator(__first, &__first, &__last);
}

template<class K, class V, class H>
typename LinkedHashTable<K, V, H>::iterator LinkedHashTable<K, V, H>::end() const {
  return iterator(nullptr, &__first, &__last);
}

}
