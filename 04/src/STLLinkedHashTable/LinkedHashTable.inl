#pragma once

#include <algorithm>
#include <execution>

#include "STLLinkedHashTable/LinkedHashTable.h"

namespace stl_lht_impl {

template<class K, class V, class H>
LinkedHashTable<K, V, H>::LinkedHashTable(std::size_t size) : __table(size, std::list<Node<const K, V>>()) {}

template<class K, class V, class H>
void LinkedHashTable<K, V, H>::put(const K& key, const V& value) {
  std::size_t hash = H::hash(key) % __table.size();

  auto it = std::find_if(std::execution::par_unseq, __table[hash].begin(), __table[hash].end(),
                         [&](auto& n) { return n.pair.first == key && n.pair.second == value; });

  if (it != __table[hash].end())
    it->pair.second = value;
  else {
    __table[hash].emplace_back(key, value, __last, nullptr);  // Node's constructor is invoked
    __last = &__table[hash].back();
    if (!__first) __first = __last;
  }
}

template<class K, class V, class H>
void LinkedHashTable<K, V, H>::remove(iterator& it) {
  auto old = it;
  --old;
  std::size_t index = H::hash(it->first) % __table.size();
  for (auto i = __table[index].begin(); i != __table[index].end(); ++i)
    if (i->pair.first == it->first && i->pair.second == it->second) {
      if (&*i == __first) __first = __first->next;
      if (&*i == __last) __last = __last->prev;
      __table[index].erase(i);  // Node's destructor is invoked
      break;
    }
  it = old;
}

template<class K, class V, class H>
typename LinkedHashTable<K, V, H>::iterator LinkedHashTable<K, V, H>::find(const V& value) const {
  return std::find_if(std::execution::par_unseq, begin(), end(), [&](auto& p) { return p.second == value; });
}

template<class K, class V, class H>
void LinkedHashTable<K, V, H>::clear() {
  for (auto& i : __table) i.clear();
  __first = __last = nullptr;
}

template<class K, class V, class H>
bool LinkedHashTable<K, V, H>::is_empty() const {
  return !__first;
}

template<class K, class V, class H>
typename LinkedHashTable<K, V, H>::iterator LinkedHashTable<K, V, H>::begin() const {
  return iterator(__first, &__first, &__last);
}

template<class K, class V, class H>
typename LinkedHashTable<K, V, H>::iterator LinkedHashTable<K, V, H>::end() const {
  return iterator(nullptr, &__first, &__last);
}

}
