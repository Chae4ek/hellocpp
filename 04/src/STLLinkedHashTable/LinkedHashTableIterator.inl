#pragma once

#include "STLLinkedHashTable/LinkedHashTableIterator.h"

namespace stl_lht_impl {

template<class K, class V>
LinkedHashTableIterator<K, V>::LinkedHashTableIterator(const LinkedHashTableIterator& h)
    : __curr(h.__curr), __first(h.__first), __last(h.__last) {}

template<class K, class V>
LinkedHashTableIterator<K, V>::LinkedHashTableIterator(const Node<K, V>* curr,
                                                       const Node<K, V>* const* first,
                                                       const Node<K, V>* const* last)
    : __curr(const_cast<Node<K, V>*>(curr)), __first(first), __last(last) {}

template<class K, class V>
inline constexpr bool LinkedHashTableIterator<K, V>::operator==(const LinkedHashTableIterator& r) const {
  return __curr == r.__curr && __first == r.__first && __last == r.__last;
}

template<class K, class V>
inline constexpr bool LinkedHashTableIterator<K, V>::operator!=(const LinkedHashTableIterator& r) const {
  return !(*this == r);
}

template<class K, class V>
LinkedHashTableIterator<K, V>& LinkedHashTableIterator<K, V>::operator++() {
  if (!__curr)
    __curr = const_cast<Node<K, V>*>(*__first);
  else
    __curr = __curr->next;
  return *this;
}

template<class K, class V>
LinkedHashTableIterator<K, V>& LinkedHashTableIterator<K, V>::operator--() {
  if (!__curr)
    __curr = const_cast<Node<K, V>*>(*__last);
  else
    __curr = __curr->prev;
  return *this;
}

template<class K, class V>
inline constexpr typename LinkedHashTableIterator<K, V>::P& LinkedHashTableIterator<K, V>::operator*() const {
  return __curr->pair;
}

template<class K, class V>
inline constexpr typename LinkedHashTableIterator<K, V>::P* LinkedHashTableIterator<K, V>::operator->()
    const {
  return &__curr->pair;
}

template<class K, class V>
inline constexpr LinkedHashTableIterator<K, V>& LinkedHashTableIterator<K, V>::operator=(
    const LinkedHashTableIterator& r) {
  __curr = const_cast<Node<K, V>*>(r.__curr);
  __first = r.__first;
  __last = r.__last;
  return *this;
}

}
