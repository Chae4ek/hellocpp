#pragma once

#include <algorithm>

#include "HashTable/HashTable.h"

namespace std_ht_impl {

template<class K, class V, class H>
HashTable<K, V, H>::HashTable(std::size_t size) : __size(size) {
  __table = new Node<const K, V>*[size];
  for (std::size_t i = 0; i < size; ++i) __table[i] = nullptr;
}

template<class K, class V, class H>
HashTable<K, V, H>::~HashTable() {
  clear();
}

template<class K, class V, class H>
void HashTable<K, V, H>::put(const K& key, const V& value) {
  std::size_t hash = H::hash(key) % __size;

  auto* cur = __table[hash];
  Node<const K, V>* prev = nullptr;
  while (cur) {
    prev = cur;
    if (cur->pair.first == key && cur->pair.second == value) break;
    cur = cur->next;
  }

  if (cur)
    cur->pair.second = value;
  else {
    auto* t = new Node<const K, V>(key, value, prev, nullptr);  // Node's constructor is invoked
    if (!__table[hash]) __table[hash] = t;
  }
}

template<class K, class V, class H>
void HashTable<K, V, H>::remove(iterator& it) {
  auto old = it;
  --old;
  std::size_t index = H::hash(it->first) % __size;
  auto* cur = __table[index];
  while (cur) {
    if (cur->pair.first == it->first && cur->pair.second == it->second) {
      if (!cur->prev) __table[index] = cur->next;
      delete cur;  // Node's destructor is invoked
      break;
    }
    cur = cur->next;
  }
  it = old;
}

template<class K, class V, class H>
typename HashTable<K, V, H>::iterator HashTable<K, V, H>::find(const V& value) const {
  return std::find_if(begin(), end(), [&](auto& p) { return p.second == value; });
}

template<class K, class V, class H>
void HashTable<K, V, H>::clear() {
  for (std::size_t i = 0; i < __size; ++i) {
    auto* cur = __table[i];
    while (cur) {
      auto* next = cur->next;
      delete cur;
      cur = next;
    }
    __table[i] = nullptr;
  }
}

template<class K, class V, class H>
bool HashTable<K, V, H>::is_empty() const {
  for (std::size_t i = 0; i < __size; ++i)
    if (__table[i]) return false;
  return true;
}

template<class K, class V, class H>
typename HashTable<K, V, H>::iterator HashTable<K, V, H>::begin() const {
  return ++iterator(__table, __size, static_cast<std::size_t>(-1), nullptr);
}

template<class K, class V, class H>
typename HashTable<K, V, H>::iterator HashTable<K, V, H>::end() const {
  return iterator(__table, __size, __size, nullptr);
}

}
