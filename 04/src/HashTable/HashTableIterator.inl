#pragma once

#include "HashTable/HashTableIterator.h"

namespace std_ht_impl {

template<class K, class V>
HashTableIterator<K, V>::HashTableIterator(const HashTableIterator& h)
    : __table(h.__table), __size(h.__size), __index(h.__index), __bucket_index(h.__bucket_index) {}

template<class K, class V>
HashTableIterator<K, V>::HashTableIterator(const Node<K, V>* const* table,
                                           std::size_t size,
                                           std::size_t index,
                                           Node<K, V>* bucket_index)
    : __table(table), __size(size), __index(index), __bucket_index(bucket_index) {}

template<class K, class V>
inline constexpr bool HashTableIterator<K, V>::operator==(const HashTableIterator& r) const {
  return __table == r.__table && __size == r.__size && __index == r.__index &&
         __bucket_index == r.__bucket_index;
}

template<class K, class V>
inline constexpr bool HashTableIterator<K, V>::operator!=(const HashTableIterator& r) const {
  return !(*this == r);
}

template<class K, class V>
HashTableIterator<K, V>& HashTableIterator<K, V>::operator++() {
  if (__index == __size) return *this;
  if (__index == static_cast<std::size_t>(-1))
    __index = 0;
  else
    __bucket_index = __bucket_index->next;

  if (!__bucket_index) {
    ++__index;
    while (__index < __size && !__table[__index]) ++__index;
    __bucket_index = __index == __size ? nullptr : const_cast<Node<K, V>*>(__table[__index]);
  }
  return *this;
}

template<class K, class V>
HashTableIterator<K, V>& HashTableIterator<K, V>::operator--() {
  if (__index == static_cast<std::size_t>(-1)) return *this;
  if (__index == __size || __bucket_index == __table[__index]) {
    --__index;
    while (__index != static_cast<std::size_t>(-1) && !__table[__index]) --__index;
    if (__index == static_cast<std::size_t>(-1))
      __bucket_index = nullptr;
    else {
      __bucket_index = const_cast<Node<K, V>*>(__table[__index]);
      while (__bucket_index && __bucket_index->next) __bucket_index = __bucket_index->next;
    }
  } else
    __bucket_index = __bucket_index->prev;
  return *this;
}

template<class K, class V>
inline constexpr typename HashTableIterator<K, V>::P& HashTableIterator<K, V>::operator*() const {
  return __bucket_index->pair;
}

template<class K, class V>
inline constexpr typename HashTableIterator<K, V>::P* HashTableIterator<K, V>::operator->() const {
  return &__bucket_index->pair;
}

template<class K, class V>
inline constexpr HashTableIterator<K, V>& HashTableIterator<K, V>::operator=(const HashTableIterator& r) {
  __table = r.__table;
  __size = r.__size;
  __index = r.__index;
  __bucket_index = r.__bucket_index;
  return *this;
}

}
