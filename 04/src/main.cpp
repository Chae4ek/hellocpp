#include <iostream>

#include "HashTable/HashTable.h"
#include "LinkedHashTable/LinkedHashTable.h"

void demo1() {
  std_ht_impl::HashTable<int, int> a;
  a.put(1, 1);
  a.put(3, 4);
  a.put(5, 5);

  for (auto i = a.begin(); i != a.end(); ++i) {
    std::cout << i->first << ' ' << i->second << std::endl;
    // i->second = 2;  // ok
    // i->first = 2;   // error
  }

  std::cout << std::endl;

  auto it = a.find(5);
  a.remove(it);
  for (auto& i : a) std::cout << i.first << ' ' << i.second << std::endl;

  std::cout << std::endl;

  a.clear();

  a.put(1, 1);
  a.put(3, 4);
  a.put(5, 5);
  for (auto i = a.begin(); i != a.end(); ++i) {
    std::cout << i->first << ' ' << i->second << std::endl;
    if (i->second == 5) {
      a.remove(i);
      a.remove(i);
      // a.remove(i);  // change if (i->second == 1)...
      std::cout << " one del " << std::endl;
    }
  }
  // equivalent to:

  // for (auto& i : a) {
  //   std::cout << i.first << ' ' << i.second << std::endl;
  //   if (i.second == 5) {
  //     auto it = a.find(i.second);
  //     a.remove(it);
  //     a.remove(it);
  //     // a.remove(it);  // change if (i->second == 1)...
  //     std::cout << " one del " << std::endl;
  //   }
  // }

  std::cout << std::endl;
  for (auto& i : a) std::cout << i.first << ' ' << i.second << std::endl;

  std::cout << std::endl << "is empty: " << a.is_empty() << std::endl;
  a.clear();
  std::cout << std::endl << "is empty: " << a.is_empty() << std::endl;
}

void demo2() {
  std_lht_impl::LinkedHashTable<int, int> a;
  a.put(1, 1);
  a.put(3, 4);
  a.put(5, 5);
  for (auto i = a.begin(); i != a.end(); ++i) {
    std::cout << i->first << ' ' << i->second << std::endl;
    // i->second = 2;  // ok
    // i->first = 2;   // error
  }

  std::cout << std::endl;

  auto it = a.find(5);
  a.remove(it);
  for (auto& i : a) std::cout << i.first << ' ' << i.second << std::endl;

  std::cout << std::endl;

  a.clear();

  a.put(1, 1);
  a.put(3, 4);
  a.put(5, 5);
  for (auto i = a.begin(); i != a.end(); ++i) {
    std::cout << i->first << ' ' << i->second << std::endl;
    if (i->second == 4) {
      a.remove(i);
      a.remove(i);
      // a.remove(i);  // change if (i->second == 5)...
      std::cout << " one del " << std::endl;
    }
  }
  // equivalent to:

  // for (auto& i : a) {
  //   std::cout << i.first << ' ' << i.second << std::endl;
  //   if (i.second == 4) {
  //     auto it = a.find(i.second);
  //     a.remove(it);
  //     a.remove(it);
  //     // a.remove(it);  // change if (i->second == 5)...
  //     std::cout << " one del " << std::endl;
  //   }
  // }

  std::cout << std::endl;
  for (auto& i : a) std::cout << i.first << ' ' << i.second << std::endl;

  std::cout << std::endl << "is empty: " << a.is_empty() << std::endl;
  a.clear();
  std::cout << std::endl << "is empty: " << a.is_empty() << std::endl;
}

int main() {
  demo1();
  std::cout << "----------- demo2 -----------\n";
  demo2();
  return 0;
}
