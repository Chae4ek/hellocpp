#pragma once

#include <list>
#include <vector>

#include "HashFunc.h"
#include "HashTableIterator.h"
#include "IHashTable.h"

namespace stl_ht_impl {

template<class K, class V, class H = HashFunc>
class HashTable : public IHashTable<K, V, HashTableIterator<const K, V>> {
 private:
  std::vector<std::list<std::pair<const K, V>>> __table;

 public:
  HashTable(std::size_t size = 127);

  using iterator = HashTableIterator<const K, V>;

  void put(const K&, const V&);
  void remove(iterator&);
  iterator find(const V&) const;
  void clear();
  bool is_empty() const;

  iterator begin() const;
  iterator end() const;
};

}

#include "STLHashTable/HashTable.inl"
