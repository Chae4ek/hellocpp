#pragma once

#include <iterator>
#include <list>
#include <vector>

namespace stl_ht_impl {

template<class K, class V>
class HashTableIterator : public std::iterator<std::bidirectional_iterator_tag, std::pair<K, V>> {
 public:
  using P = std::pair<K, V>;
  using U = std::list<P>;
  using T = std::vector<U>;

 private:
  const T* __table = nullptr;
  std::size_t __index = 0;
  typename U::const_iterator __bucket_index;

 public:
  HashTableIterator(const HashTableIterator&);
  HashTableIterator(const T& table, std::size_t index, typename U::const_iterator bucket_index);

  inline constexpr bool operator==(const HashTableIterator&) const;
  inline constexpr bool operator!=(const HashTableIterator&) const;
  HashTableIterator& operator++();
  HashTableIterator& operator--();
  inline constexpr P& operator*() const;
  inline constexpr P* operator->() const;
  inline constexpr HashTableIterator& operator=(const HashTableIterator&);
};

}

#include "STLHashTable/HashTableIterator.inl"
