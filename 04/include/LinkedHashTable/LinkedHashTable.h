#pragma once

#include "HashFunc.h"
#include "IHashTable.h"
#include "LinkedHashTableIterator.h"
#include "Node.h"

namespace std_lht_impl {

template<class K, class V, class H = HashFunc>
class LinkedHashTable : public IHashTable<K, V, LinkedHashTableIterator<const K, V>> {
 private:
  Node<const K, V>** __table;
  std::size_t __size;
  Node<const K, V>* __first = nullptr;
  Node<const K, V>* __last = nullptr;

 public:
  LinkedHashTable(std::size_t size = 127);
  ~LinkedHashTable();

  using iterator = LinkedHashTableIterator<const K, V>;

  void put(const K&, const V&);
  void remove(iterator&);
  iterator find(const V&) const;
  void clear();
  bool is_empty() const;

  iterator begin() const;
  iterator end() const;
};

}

#include "LinkedHashTable/LinkedHashTable.inl"
