#pragma once

#include <utility>

namespace std_lht_impl {

template<class K, class V>
struct Node {
  std::pair<K, V> pair;
  Node* bucket_prev = nullptr;
  Node* bucket_next = nullptr;
  Node* global_prev = nullptr;
  Node* global_next = nullptr;
  Node(K key, V value, Node* bucket_prev, Node* bucket_next, Node* global_prev, Node* global_next)
      : pair(key, value),
        bucket_prev(bucket_prev),
        bucket_next(bucket_next),
        global_prev(global_prev),
        global_next(global_next) {
    if (bucket_prev) bucket_prev->bucket_next = this;
    if (bucket_next) bucket_next->bucket_prev = this;
    if (global_prev) global_prev->global_next = this;
    if (global_next) global_next->global_prev = this;
  }
  ~Node() {
    if (bucket_prev) bucket_prev->bucket_next = bucket_next;
    if (bucket_next) bucket_next->bucket_prev = bucket_prev;
    if (global_prev) global_prev->global_next = global_next;
    if (global_next) global_next->global_prev = global_prev;
  }
};

}
