#pragma once

#include <iterator>
#include <list>
#include <vector>

#include "Node.h"

namespace stl_lht_impl {

template<class K, class V>
class LinkedHashTableIterator : public std::iterator<std::bidirectional_iterator_tag, std::pair<K, V>> {
 public:
  using P = std::pair<K, V>;

 private:
  Node<K, V>* __curr = nullptr;
  const Node<K, V>* const* __first = nullptr;
  const Node<K, V>* const* __last = nullptr;

 public:
  LinkedHashTableIterator(const LinkedHashTableIterator&);
  LinkedHashTableIterator(const Node<K, V>* curr,
                          const Node<K, V>* const* first,
                          const Node<K, V>* const* last);

  inline constexpr bool operator==(const LinkedHashTableIterator&) const;
  inline constexpr bool operator!=(const LinkedHashTableIterator&) const;
  LinkedHashTableIterator& operator++();
  LinkedHashTableIterator& operator--();
  inline constexpr P& operator*() const;
  inline constexpr P* operator->() const;
  inline constexpr LinkedHashTableIterator& operator=(const LinkedHashTableIterator&);
};

}

#include "STLLinkedHashTable/LinkedHashTableIterator.inl"
