#pragma once

#include <type_traits>

struct HashFunc {
  template<class K>
  static inline std::size_t hash(K key) {
    std::size_t h = 0;
    while (key) {
      h += key % 10;
      key /= 10;
    }
    return h;
  }
};
