#pragma once

template<class K, class V, class iterator>
class IHashTable {
 public:
  virtual void put(const K&, const V&) = 0;
  virtual void remove(iterator&) = 0;
  virtual iterator find(const V&) const = 0;
  virtual void clear() = 0;
  virtual bool is_empty() const = 0;

  virtual iterator begin() const = 0;
  virtual iterator end() const = 0;
};
