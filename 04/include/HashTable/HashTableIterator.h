#pragma once

#include <iterator>

#include "Node.h"

namespace std_ht_impl {

template<class K, class V>
class HashTableIterator : public std::iterator<std::bidirectional_iterator_tag, std::pair<K, V>> {
 public:
  using P = std::pair<K, V>;

 private:
  const Node<K, V>* const* __table = nullptr;
  std::size_t __size = 0;
  std::size_t __index = 0;
  Node<K, V>* __bucket_index = nullptr;

 public:
  HashTableIterator(const HashTableIterator&);
  HashTableIterator(const Node<K, V>* const* table,
                    std::size_t size,
                    std::size_t index,
                    Node<K, V>* bucket_index);

  inline constexpr bool operator==(const HashTableIterator&) const;
  inline constexpr bool operator!=(const HashTableIterator&) const;
  HashTableIterator& operator++();
  HashTableIterator& operator--();
  inline constexpr P& operator*() const;
  inline constexpr P* operator->() const;
  inline constexpr HashTableIterator& operator=(const HashTableIterator&);
};

}

#include "HashTable/HashTableIterator.inl"
