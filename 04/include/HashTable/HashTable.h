#pragma once

#include "HashFunc.h"
#include "HashTableIterator.h"
#include "IHashTable.h"
#include "Node.h"

namespace std_ht_impl {

template<class K, class V, class H = HashFunc>
class HashTable : public IHashTable<K, V, HashTableIterator<const K, V>> {
 private:
  Node<const K, V>** __table;
  std::size_t __size;

 public:
  HashTable(std::size_t size = 127);
  ~HashTable();

  using iterator = HashTableIterator<const K, V>;

  void put(const K&, const V&);
  void remove(iterator&);
  iterator find(const V&) const;
  void clear();
  bool is_empty() const;

  iterator begin() const;
  iterator end() const;
};

}

#include "HashTable/HashTable.inl"
