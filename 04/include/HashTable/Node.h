#pragma once

#include <utility>

namespace std_ht_impl {

template<class K, class V>
struct Node {
  std::pair<K, V> pair;
  Node* prev = nullptr;
  Node* next = nullptr;
  Node(K key, V value, Node* prev, Node* next) : pair(key, value), prev(prev), next(next) {
    if (prev) prev->next = this;
    if (next) next->prev = this;
  }
  ~Node() {
    if (prev) prev->next = next;
    if (next) next->prev = prev;
  }
};

}
